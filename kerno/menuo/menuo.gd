extends Node


var load_scene : String


func _on_Profilo_pressed():
	get_tree().change_scene('res://blokoj/profilo/profilo.tscn')


func _on_Resurso_Center_pressed():
	$CanvasLayer/UI/Lbar/L_VBox/RCentro/Window.popup_centered()


func _on_Objektoj_pressed():
	$CanvasLayer/UI/Lbar/L_VBox/Objektoj/Window.set_visible(true)
	
	
func _on_Servilo_pressed():
	get_tree().change_scene('res://blokoj/servilo/servilo.tscn')

func _on_Kosmo_pressed():
	get_tree().change_scene('res://blokoj/kosmo/space.tscn')


func set_visible(visible: bool):
	$CanvasLayer/UI.visible = visible
	

func _on_Taskoj_pressed():
	$CanvasLayer/UI/Lbar/L_VBox/Taskoj/Window.set_visible(true)



func _on_cap_pressed():
	get_tree().change_scene('res://blokoj/kosmostacio/CapKosmostacio.tscn')


func _on_com_pressed():
	get_tree().change_scene("res://blokoj/kosmostacio/ComKosmostacio.tscn")


func _on_real_pressed():
	get_tree().change_scene("res://blokoj/kosmostacio/Kosmostacio.tscn")
