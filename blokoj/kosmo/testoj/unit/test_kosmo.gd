extends "res://addons/gut/test.gd"

var main_sim = preload("res://blokoj/kosmo/kosmo.tscn") # помещаю в переменную сцену 


func test_fly_out_station():
	var curnt_sim = main_sim.instance() # создаю инстанц сцены
	add_child(curnt_sim) # добавляют загруженую сцену в дерево текущейго проекта,
	print(curnt_sim) # контроль, можно удалить
	assert_between(get_node('kosmo').delay1, 3, 6, 'It should be between 3-6 minutes') # вызываем значение переменноий delay1 из узла kosmo и проверяем текущее значение в переменной с заданным в ТЗ.
	remove_child(curnt_sim) # удаляем из дерева проекта ранее загруженную сцену

func test_fly_to_asteroids():
	var curnt_sim = main_sim.instance()
	add_child(curnt_sim)
	print(curnt_sim.delay2)	
	var summ = get_node('kosmo').delay2 - get_node("kosmo").delay1 #в сцене kosmo в функции _ready происходит сложение текущего значения переменной delay с предыдущим, для получения "чистого" значения делаем обратное действие
	assert_between(summ, 21, 27, 'It should be between 21-27 minutes') 
	remove_child(curnt_sim)

func test_rock_crushing_asteroid():
	var curnt_sim = main_sim.instance()
	add_child(curnt_sim)
	print(curnt_sim.delay3)	
	var summ = get_node("kosmo").delay3 - get_node("kosmo").delay2
	assert_between(summ, 9, 15, 'It should be between 9-15 minutes') 
	remove_child(curnt_sim)

func test_collect_crushing_asteroid():
	var curnt_sim = main_sim.instance()
	add_child(curnt_sim)
	print(curnt_sim.delay4)	
	var summ = get_node("kosmo").delay4 - get_node("kosmo").delay3
	assert_between(summ, 6, 9, 'It should be between 6-9 minutes') 
	remove_child(curnt_sim)	

func test_fly_back_to_station():
	var curnt_sim = main_sim.instance()
	add_child(curnt_sim)
	print(curnt_sim.delay5)	
	var summ = get_node("kosmo").delay5 - get_node("kosmo").delay4
	assert_between(summ, 21, 27, 'It should be between 21-27 minutes') 
	remove_child(curnt_sim)		

