extends Control


func _on_Close_button_pressed():
	set_visible(false)
	
	
func _resize(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$MarginContainer.rect_size += event.relative


func _drag(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$MarginContainer.rect_position += event.relative
	

