extends Button


const QueryObject = preload("queries.gd")


# Вызывается, когда запрос сохранения профиля выполнен
func save_profile_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	
	# TODO: добавить обработку ответа
	
	# Если запрос выполнен успешно
	if parsed_resp['data']['redaktuUniversoUzanto']['status']:
		# Обновляем никнейм и uuid записи в глобальном объекте
		Global.nickname = parsed_resp['data']['redaktuUniversoUzanto']['universoUzanto']['retnomo']['enhavo']
		Global.nickname_uuid = parsed_resp['data']['redaktuUniversoUzanto']['universoUzanto']['uuid']
		# Переходим на сцену с видом на космостанцию
		get_tree().change_scene('res://blokoj/kosmostacio/Kosmostacio.tscn')


# Вызывается при нажатии кнопки Сохранить
func _pressed():
	# Получаем никнейм из поля ввода
	var nickname = $'../NicknameEdit'.text
	
	# Если никнейм пустой
	if !nickname:
		# Показываем ошибку
		$'../NicknameErrorLabel'.show()
	# Если поле с никнеймом заполнено
	else:
		# Прячем ошибку
		$'../NicknameErrorLabel'.hide()
		
		# Если никнейм не меняли
		if nickname == Global.nickname:
			# Выходим на сцену с видом на космостанцию
			get_tree().change_scene('res://blokoj/kosmostacio/Kosmostacio.tscn')
			return
		
		# В остальных случаях
		# TODO: подумать нужна ли эта проверка?
		# Если авторизованы
		if Global.status:
			# Объект с данными для запроса (сохранить профиль)
			var q = QueryObject.new()
			# Объект запроса
			var save_profile_request = HTTPRequest.new()
			# Здесь будут данные для сохранения (передаются в запрос)
			var profile = {}
			
			# Никнейм
			profile['nickname'] = nickname
			
			# Если никнейм уже был
			if Global.nickname_uuid:
				# заполняем uuid, чтобы запрос обновил поля
				profile['uuid'] = ', uuid: "' + Global.nickname_uuid + '"'
			# В противном случае uuid не нужен
			else:
				profile['uuid'] = ''
			
			# Добавляем объект запроса в дерево сцены
			add_child(save_profile_request)
			
			# Связываем сигнал о завершении запроса с обработчиком
			save_profile_request.connect("request_completed", self, "save_profile_completed")
			
			# Посылаем запрос
			var error = save_profile_request.request(q.URL_DATA, Global.backend_headers, true, 2, q.save_profile_query(profile))
			
			# Если запрос не выполнен выводим ошибку в консоль
			# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
			if error != OK:
				print('Error in Save Profile Request.')
