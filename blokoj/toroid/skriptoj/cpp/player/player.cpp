#include "player.h"

using namespace godot;

void Player::_register_methods() {
    register_method("_physics_process", &Player::_physics_process);
}

Player::Player() {

}

Player::~Player() {

}

void Player::_init() {

}

void Player::_physics_process(float delta) {
    Transform t;
    Vector3 dir = Vector3();
    Input* input = Input::get_singleton();
    if (input->is_action_pressed("ui_left")) {
        rotate_object_local(Vector3(0, 0, 1), -delta);
    }
    if (input->is_action_pressed("ui_right")) {
        rotate_object_local(Vector3(0, 0, 1), delta);
    }
    if (input->is_action_pressed("ui_down")) {
        rotate_object_local(Vector3(1, 0, 0), delta);
    }
    if (input->is_action_pressed("ui_up")) {
        rotate_object_local(Vector3(1, 0, 0), -delta);
    }
    if (input->is_action_pressed("move_forward")) {
        t = get_transform();
        dir += t.basis.z.normalized();
    }
    if (input->is_action_pressed("move_back")) {
        t = get_transform();
        dir -= t.basis.z.normalized();
    }

    if (dir.x != 0 or dir.y != 0 or dir.z != 0) {
        dir *= SPEED * delta;
        move_and_slide(dir, Vector3(0, 0, 1));
    }
}
